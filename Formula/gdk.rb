class Gdk < Formula
  desc "Gitlab GDK"
  homepage "https://gitlab.com/"
  url "https://gitlab.com/alexpooley/homebrew-gitlab/Formula/gdk.rb"
  version "0.1"

  uses_from_macos "icu4c"
  uses_from_macos "libiconv"

  depends_on "cmake"
  depends_on "coreutils"
  depends_on "exiftool"
  depends_on "gnupg"
  depends_on "go"
  depends_on "graphicsmagick"
  depends_on "node@12"
  depends_on "openssl"
  depends_on "pkg-config"
  depends_on "postgresql@10"
  depends_on "re2"
  depends_on "redis"
  depends_on "runit"
  depends_on "yarn"
  depends_on "rbenv"

  def install
    system brew_path, "link", "pkg-config"
    system brew_path, "pin", "node@12"
    system brew_path, "pin", "icu4c"
    system brew_path, "pin", "readline"
    system *%w[bundle config build.eventmachine --with-cppflags=-I/usr/local/opt/openssl/include]
    system brew_path, "cask", "install", "google-chrome"
    system brew_path, "cask", "install", "chromedriver"

    prepend_path_in_profile "/usr/local/opt/postgresql@10/bin:/usr/local/opt/node@12/bin"
    set_variable_in_profile "PKG_CONFIG_PATH", "/usr/local/opt/icu4c/lib/pkgconfig:$PKG_CONFIG_PATH"
  end

  test do
    # noop
  end

  def brew_path
    "/usr/local/bin/brew"
  end

end
